SDS INDDUCTION PROJECT:

CORONA TRACKING APPLICATION USING ANDROID STUDIO CODE

Following are the instructions to run the application

1) All the coding is done using Android Studio code.

2) MainActivity.java is the main file where the major part of coding is done.

3) activity_main.xml is the main file where all the design of the application is carried out.The layout of the page in the application is deginedin this file.

4) All the dependencies are added in the build.gradle(module: app) and then they are synchronised.

5) To run the application on the mobile phone, following steps should be followed:-

a. Connect the Android mobile phone to the PC through the USB cable.

b. Go to Settings, More Settings, About Phone.

c. Click on the Software Version for 7 times. Message will be displayed "You are a Developer."

d. Go to More Settings, Developer options.

e. Enable the USB Debugging.

f. The mobile will be connected to the PC.

g. Run the code.

h. The application "GoCoronaGo" will be installed on the android mobile.
  
